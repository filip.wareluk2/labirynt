/*
In this file rading and writing maze files will be handled
Maze will be 2d array of size 20x20
Mazes are stored in txt file
Eery filed in maze is coded as follows:
w - wall, obstacle for player in maze
s - player position - initially at the start
p - path
f - finnish
b - buka
*/
import contentsMaze1 from "../maps/maze1";
import contentsMaze2 from "../maps/maze2";
import contentsMaze3 from "../maps/maze3";
import contentsMaze4 from "../maps/maze4";
import contentsMaze5 from "../maps/maze5";
import { config } from "../const/config";

export default function readMaze(level) {
  const mazeSize = config.mazeSize;

  var contents = getContentsForLevel(level);

  var maze = createMazeArray(mazeSize);

  for (let i = 0; i < mazeSize; i++) {
    for (let j = 0; j < mazeSize; j++) {
      maze[i][j] = contents.substr(i * mazeSize + j, 1);
    }
  }

  return maze;
}

function createMazeArray(mazeSize) {
  var maze = new Array(mazeSize);

  for (var i = 0; i < maze.length; i++) {
    maze[i] = new Array(mazeSize);
  }
  return maze;
}

function getContentsForLevel(level) {
  if (level === 1) {
    return contentsMaze1;
  }
  if (level === 2) {
    return contentsMaze2;
  }
  if (level === 3) {
    return contentsMaze3;
  }
  if (level === 4) {
    return contentsMaze4;
  }
  if (level === 5) {
    return contentsMaze5;
  }
}
