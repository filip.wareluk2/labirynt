import { useState, useEffect } from "react";
import { Audio } from "expo-av";

export default function BukaSoundPlayer({ bukaCoordinate, movmentCallBack }) {
  const [direction, setDirection] = useState([]);
  const [isPlaying, setIsPlaying] = useState(false);

  const angle = bukaCoordinate[1];
  const distance = Math.abs(bukaCoordinate[0]).toFixed(1) / 30;
  const volume = (1 - distance) / 3;

  

  useEffect(() => {
    let txt = "";
    let directiontxt = "";
    let tempArray = [];
    if (angle < 140 && angle >= 0) {
      tempArray.push("r");
      txt += "r";
    }
    if (angle < 359 && angle >= 210) {
      tempArray.push("l");
      txt += "l";
    }
    if (angle < 45 || angle > 315) {
      tempArray.push("u");
      txt += "u";
    }
    if (angle < 225 && angle >= 135) {
      if (distance <= 0.08) {
        
        tempArray.push("u");
        txt += "u";
      }
      tempArray.push("b");
      txt += "b";
    }

    if (direction) {
      direction.forEach((element) => {
        directiontxt += element;
      });
    }

    if (directiontxt !== txt) {
      setDirection(tempArray);
    }
  }, [bukaCoordinate]);

  useEffect(() => {
    if (direction[0] && isPlaying == false && Math.abs(bukaCoordinate[0]) < 8) {
      Player({ direction, setIsPlaying, volume });
      setIsPlaying(true);
    }
  }, [bukaCoordinate]);

  return null;
}

async function Player({ direction, setIsPlaying, volume }) {
  direction.forEach((element) => {
    let path = ResolvePath(element);
    playSound({ path, setIsPlaying, volume });
  });
}

async function playSound({ path, setIsPlaying, volume }) {
  const { sound: soundObject } = await Audio.Sound.createAsync(path);
  let status;
  soundObject.setVolumeAsync(volume);
  status = await soundObject.getStatusAsync();
  if (status != undefined) {
    var duration = status.durationMillis;
  }

  if (duration) {
    setTimeout(async function () {
      soundObject.unloadAsync();
      setIsPlaying(false);
    }, duration + 300);
  }

  await soundObject.playAsync();
}

function ResolvePath(direction) {
  let rand = Math.random();

  if (rand <= 0.33) {
    if (direction == "r") {
      return path.zombie1R;
    }
    if (direction == "l") {
      return path.zombie1L;
    }
    if (direction == "b") {
      return path.zombie1B;
    }
    return path.zombie1;
  }
  if (rand < 0.66 && rand > 0.33) {
    if (direction == "r") {
      return path.zombie2R;
    }
    if (direction == "l") {
      return path.zombie2L;
    }
    if (direction == "b") {
      return path.zombie2B;
    }
    return path.zombie2;
  }
  if (rand >= 0.66) {
    if (direction == "r") {
      return path.zombie3R;
    }
    if (direction == "l") {
      return path.zombie3L;
    }
    if (direction == "b") {
      return path.zombie3B;
    }
    return path.zombie3;
  }
}

const path = {
  zombie1: require("../assets/sounds/zombie/zombie1.wav"),
  zombie1L: require("../assets/sounds/zombie/zombie1L.wav"),
  zombie1R: require("../assets/sounds/zombie/zombie1R.wav"),
  zombie1B: require("../assets/sounds/zombie/zombie1B.wav"),

  zombie2: require("../assets/sounds/zombie/zombie2.wav"),
  zombie2L: require("../assets/sounds/zombie/zombie2L.wav"),
  zombie2R: require("../assets/sounds/zombie/zombie2R.wav"),
  zombie2B: require("../assets/sounds/zombie/zombie2B.wav"),

  zombie3: require("../assets/sounds/zombie/zombie3.wav"),
  zombie3L: require("../assets/sounds/zombie/zombie3L.wav"),
  zombie3R: require("../assets/sounds/zombie/zombie3R.wav"),
  zombie3B: require("../assets/sounds/zombie/zombie3B.wav"),
};
