import { useState, useEffect } from "react";
import { Audio } from "expo-av";

export default function FinishSoundPlayer({ finishCoordinate }) {
  const [direction, setDirection] = useState([]);
  const [isPlaying, setIsPlaying] = useState(false);

  const angle = finishCoordinate[1];
  const distance = Math.abs(finishCoordinate[0]).toFixed(1) / 30;
  const volume = (1 - distance) / 3;

  useEffect(() => {
    let txt = "";
    let directiontxt = "";
    let tempArray = [];
    if (angle < 140 && angle >= 45) {
      tempArray.push("r");
      txt += "r";
    }
    if (angle < 359 && angle >= 210) {
      tempArray.push("l");
      txt += "l";
    }
    if (angle <= 45 || angle > 315) {
      tempArray.push("u");
      txt += "u";
    }
    if (angle < 225 && angle >= 135) {
      tempArray.push("b");
      txt += "b";
    }

    if (direction) {
      direction.forEach((element) => {
        directiontxt += element;
      });
    }

    if (directiontxt !== txt) {
      setDirection(tempArray);
    }
  }, [finishCoordinate]);

  useEffect(() => {
    if (
      direction[0] &&
      isPlaying == false &&
      Math.abs(finishCoordinate[0]) < 10
    ) {
      Player({
        direction,
        setIsPlaying,
        volume,
        finishCoordinate: finishCoordinate[0],
      });
      setIsPlaying(true);
    }
  }, [finishCoordinate]);

  return null;
}

async function Player({ direction, setIsPlaying, volume, finishCoordinate }) {
  direction.forEach((element) => {
    let path = ResolvePath(element, finishCoordinate);
    playSound({ path, setIsPlaying, volume });
  });
}

async function playSound({ path, setIsPlaying, volume }) {
  const { sound: soundObject } = await Audio.Sound.createAsync(path);
  let status;
  soundObject.setVolumeAsync(volume);
  status = await soundObject.getStatusAsync();
  if (status != undefined) {
    var duration = status.durationMillis;
  }

  if (duration) {
    setTimeout(async function () {
      soundObject.unloadAsync();
      setIsPlaying(false);
    }, duration + 300);
  }

  await soundObject.playAsync();
}

function ResolvePath(direction, finishCoordinate) {
  let coordinateNew = Math.abs(finishCoordinate);

  if (coordinateNew > 6) {
    if (direction == "r") {
      return path.finish1R;
    }
    if (direction == "l") {
      return path.finish1L;
    }
    if (direction == "b") {
      return path.finish1B;
    }
    return path.finish1;
  }
  if (coordinateNew > 3 && coordinateNew <= 6) {
    if (direction == "r") {
      return path.finish2R;
    }
    if (direction == "l") {
      return path.finish2L;
    }
    if (direction == "b") {
      return path.finish2B;
    }
    return path.finish2;
  }
  if (coordinateNew <= 3) {
    if (direction == "r") {
      return path.finish3R;
    }
    if (direction == "l") {
      return path.finish3L;
    }
    if (direction == "b") {
      return path.finish3B;
    }
    return path.finish3;
  }
}

const path = {
  finish1: require("../assets/sounds/finish/finish1.wav"),
  finish1L: require("../assets/sounds/finish/finish1L.wav"),
  finish1R: require("../assets/sounds/finish/finish1R.wav"),
  finish1B: require("../assets/sounds/finish/finish1B.wav"),

  finish2: require("../assets/sounds/finish/finish2.wav"),
  finish2L: require("../assets/sounds/finish/finish2L.wav"),
  finish2R: require("../assets/sounds/finish/finish2R.wav"),
  finish2B: require("../assets/sounds/finish/finish2B.wav"),

  finish3: require("../assets/sounds/finish/finish3.wav"),
  finish3L: require("../assets/sounds/finish/finish3L.wav"),
  finish3R: require("../assets/sounds/finish/finish3R.wav"),
  finish3B: require("../assets/sounds/finish/finish3B.wav"),
};
