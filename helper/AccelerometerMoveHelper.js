import { useState, useEffect } from "react";
import { Accelerometer } from "expo-sensors";

export default function AccelerometerMoveHelper({
  setMovmentCallBack,
  setRestartReverse,
}) {
  Accelerometer.setUpdateInterval(1000);

  const [data, setData] = useState({
    x: 0,
    y: 0,
    z: 0,
  });
  const [subscription, setSubscription] = useState(null);

  //console.log("Y: " + Math.floor(data.y * 100) / 100 + "X: " + Math.floor(data.x * 100) / 100 + "Z: "+ Math.floor(data.z * 100) / 100);

  useEffect(() => {
    if (data.y > 0.25 && data.y < 0.75) {
      setMovmentCallBack("d");
    } else if (data.y < -0.25) {
      setMovmentCallBack("u");
    }

    if (data.x > 0.25) {
      setMovmentCallBack("l");
    } else if (data.x < -0.25) {
      setMovmentCallBack("r");
    }
    if (data.z < -0.9) {
      setRestartReverse(true);
    }
  }, [data]);

  const _subscribe = () => {
    setSubscription(
      Accelerometer.addListener((accelerometerData) => {
        setData(accelerometerData);
      })
    );
  };

  const _unsubscribe = () => {
    subscription && subscription.remove();
    setSubscription(null);
  };

  useEffect(() => {
    _subscribe();
    return () => _unsubscribe();
  }, []);

  const { x, y, z } = data;
  return null;
}
