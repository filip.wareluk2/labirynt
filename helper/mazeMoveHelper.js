import { config } from "../const/config";
import readMaze from "./mazeFilesHelper";
import { findPlayer } from "./positionHelper";
import {
  NotificationFeedbackType,
  ImpactFeedbackStyle,
  notificationAsync,
  impactAsync,
} from "expo-haptics";

var mazeSize = config.mazeSize;

export default function movePlayer(direction, moveProps) {
  if (direction == "u") {
    moveUp(moveProps);
  } else if (direction == "d") {
    moveDown(moveProps);
  } else if (direction == "l") {
    moveLeft(moveProps);
  } else {
    moveRight(moveProps);
  }
}

function moveUp(moveProps) {
  const {
    playerPosition,
    setPlayerPosition,
    maze,
    setMaze,
    level,
    setLevel,
    navigation,
  } = moveProps;

  console.log("Ruch w góre");
  let positionY = playerPosition.playerY - 1;
  let positionX = playerPosition.playerX;

  if (
    !!validateMove(
      maze,
      positionX,
      positionY,
      level,
      setLevel,
      setMaze,
      setPlayerPosition,
      navigation
    )
  ) {
    impactAsync(ImpactFeedbackStyle.Light);
    setPlayerPosition(
      (prevState) => {
        return {
          ...prevState,
          playerY: positionY,
        };
      },
      [mazeUpdate({ maze, setMaze, positionY, positionX })]
    );
  }
}

function moveDown(moveProps) {
  const {
    playerPosition,
    setPlayerPosition,
    maze,
    setMaze,
    level,
    setLevel,
    navigation,
  } = moveProps;

  console.log("Ruch w dół");
  let positionY = playerPosition.playerY + 1;
  let positionX = playerPosition.playerX;

  if (
    !!validateMove(
      maze,
      positionX,
      positionY,
      level,
      setLevel,
      setMaze,
      setPlayerPosition,
      navigation
    )
  ) {
    impactAsync(ImpactFeedbackStyle.Light);
    setPlayerPosition(
      (prevState) => {
        return {
          ...prevState,
          playerY: positionY,
        };
      },
      [mazeUpdate({ maze, setMaze, positionY, positionX })]
    );
  }
}

function moveLeft(moveProps) {
  const {
    playerPosition,
    setPlayerPosition,
    maze,
    setMaze,
    level,
    setLevel,
    navigation,
  } = moveProps;

  console.log("Ruch w lewo");
  let positionY = playerPosition.playerY;
  let positionX = playerPosition.playerX - 1;

  if (
    !!validateMove(
      maze,
      positionX,
      positionY,
      level,
      setLevel,
      setMaze,
      setPlayerPosition,
      navigation
    )
  ) {
    impactAsync(ImpactFeedbackStyle.Light);
    setPlayerPosition(
      (prevState) => {
        return {
          ...prevState,
          playerX: positionX,
        };
      },
      [mazeUpdate({ maze, setMaze, positionY, positionX })]
    );
  }
}

function moveRight(moveProps) {
  const {
    playerPosition,
    setPlayerPosition,
    maze,
    setMaze,
    level,
    setLevel,
    navigation,
  } = moveProps;

  console.log("Ruch w prawo");
  let positionY = playerPosition.playerY;
  let positionX = playerPosition.playerX + 1;

  if (
    !!validateMove(
      maze,
      positionX,
      positionY,
      level,
      setLevel,
      setMaze,
      setPlayerPosition,
      navigation
    )
  ) {
    impactAsync(ImpactFeedbackStyle.Light);
    setPlayerPosition(
      (prevState) => {
        return {
          ...prevState,
          playerX: positionX,
        };
      },
      [mazeUpdate({ maze, setMaze, positionY, positionX })]
    );
  }
}

function mazeUpdate({ maze, setMaze, positionY, positionX }) {
  let tempMaze = maze;
  for (let i = 0; i < mazeSize; i++) {
    for (let j = 0; j < mazeSize; j++) {
      if (tempMaze[i][j] == "s") {
        tempMaze[i][j] = "p";
      }
      if (i == positionY && j == positionX) {
        tempMaze[i][j] = "s";
      }
    }
  }
  setMaze(tempMaze);
}

function validateMove(
  maze,
  newX,
  newY,
  level,
  setLevel,
  setMaze,
  setPlayerPosition,
  navigation
) {
  if (newX < 0 || newX >= mazeSize || newY < 0 || newY >= mazeSize) {
    console.log("Player out of bounds");
    notificationAsync(NotificationFeedbackType.Warning);
    return false;
  }
  if (maze[newY][newX] === "w") {
    console.log("Wall");
    notificationAsync(NotificationFeedbackType.Warning);
    return false;
  }
  if (maze[newY][newX] === "f") {
    if (level === 5) {
      notificationAsync(NotificationFeedbackType.Success);
      notificationAsync(NotificationFeedbackType.Success);
      navigation("Win");
    } else {
      notificationAsync(NotificationFeedbackType.Success);
      notificationAsync(NotificationFeedbackType.Success);
      console.log("Level complete!");
      console.log("Level " + level);
      let newMaze = readMaze(level + 1);
      findPlayer({ newMaze, setPlayerPosition });
      setMaze(newMaze);
      setLevel(level + 1);
    }

    return false;
  }
  if (maze[newY][newX] === "b") {
    notificationAsync(NotificationFeedbackType.Success);
    console.log("Buka found! Game finnished!");
    navigation("WinBuka");
  }
  return true;
}
