import React from "react";
import { StyleSheet, View } from "react-native";

export default function DisplayMaze({ arrey }) {
  return (
    <View
      style={{
        flex: 3,
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <View
        style={{
          borderWidth: 2,
          borderColor: "grey",
        }}
      >
        {arrey.map((currentRow, rowIndex) => {
          return (
            <View
              style={{
                marginVertical: 1,
                flexDirection: "row",
              }}
              key={rowIndex}
            >
              {currentRow.map((currentItem, itemIndex) => {
                return (
                  <View
                    key={itemIndex}
                    style={[
                      { marginHorizontal: 1 },
                      currentItem == "p"
                        ? [styles.wall, styles.common]
                        : currentItem == "w"
                        ? [styles.wall, styles.common]
                        : currentItem == "b"
                        ? [styles.buka, styles.common]
                        : currentItem == "f"
                        ? [styles.finish, styles.common]
                        : [styles.player, styles.common],
                    ]}
                  />
                );
              })}
            </View>
          );
        })}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  common: { width: 15, height: 15 },
  buka: { backgroundColor: "red" },
  wall: { backgroundColor: "black" },
  path: { backgroundColor: "white" },
  player: { backgroundColor: "green" },
  finish: { backgroundColor: "gold" },
});
