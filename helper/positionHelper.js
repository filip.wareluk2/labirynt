import { config } from "../const/config";

var mazeSize = config.mazeSize;

function findByObjectType({ maze, objectType }) {
  for (let i = 0; i < mazeSize; i++) {
    for (let j = 0; j < mazeSize; j++) {
      if (maze[i][j] == objectType) {
        return [i, j];
      }
    }
  }
}

export function findPlayer({ newMaze, setPlayerPosition }) {
  for (let i = 0; i < mazeSize; i++) {
    for (let j = 0; j < mazeSize; j++) {
      if (newMaze[i][j] == "s") {
        setPlayerPosition({ playerX: j, playerY: i });
      }
    }
  }
}

export default function calculateDistance({ maze, objectType }) {
  let playerPosition = findByObjectType({ maze, objectType: "s" });
  let objectPosition = findByObjectType({ maze, objectType });

  if(!playerPosition || !objectPosition){
    return [];
  }

  let distanceY = objectPosition[0] - playerPosition[0];
  let distanceX = objectPosition[1] - playerPosition[1];

  let coordinateArray = [];

  let angleDeg = 180 - (Math.atan2(distanceX, distanceY) * 180) / Math.PI;

  if (distanceX == 0) {
    coordinateArray.push(distanceY);
    coordinateArray.push(angleDeg.toFixed(5));

    return coordinateArray;
  } else if (distanceY == 0) {
    coordinateArray.push(distanceX);
    coordinateArray.push(angleDeg.toFixed(5));

    return coordinateArray;
  }

  let distance = Math.sqrt(Math.pow(distanceX, 2) + Math.pow(distanceY, 2));

  coordinateArray.push(distance.toFixed(5));
  coordinateArray.push(angleDeg.toFixed(5));

  return coordinateArray;
}
