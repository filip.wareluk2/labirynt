import { useState, useEffect } from "react";
import { Audio } from "expo-av";

export default function MusichSoundPlayer({ finishCoordinate }) {
  const [isPlaying, setIsPlaying] = useState(false);

  var volume = 0.1;

  useEffect(() => {
    if (isPlaying == false) {
      Player({
        setIsPlaying,
        volume,
      });
      setIsPlaying(true);
    }
  }, [finishCoordinate]);

  return null;
}

async function Player({ setIsPlaying, volume }) {
  playSound({ path: path.music, setIsPlaying, volume });
}

async function playSound({ path, setIsPlaying, volume }) {
  const { sound: soundObject } = await Audio.Sound.createAsync(path);
  let status;
  soundObject.setVolumeAsync(volume);
  status = await soundObject.getStatusAsync();

  setTimeout(async function () {
    soundObject.unloadAsync();
    setIsPlaying(false);
  }, 297000);

  await soundObject.playAsync();
}

const path = {
  music: require("../assets/sounds/music/music.aac"),
};
