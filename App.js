import React from "react";
import { StatusBar } from "expo-status-bar";
import { StyleSheet } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import MainMenu from "./screens/MainMenu";
import InstructionsSoundPlayer from "./screens/Instructions";
import Game from "./screens/Game";
import Win from "./screens/Win";
import WinBuka from "./screens/WinBuka";

import { LogBox } from "react-native";

LogBox.ignoreLogs([
  "Non-serializable values were found in the navigation state",
  "Setting a timer",
]);

const Stack = createNativeStackNavigator();
const screenOptions = { header: () => null };

export default function App() {
  return (
    <NavigationContainer>
      <StatusBar barStyle="dark-content" />
      <Stack.Navigator screenOptions={screenOptions}>
        <Stack.Screen name="MainMenu" component={MainMenu} initialRouteName />
        <Stack.Screen name="Game" component={Game} />
        <Stack.Screen name="Win" component={Win} />
        <Stack.Screen name="WinBuka" component={WinBuka} />
        <Stack.Screen name="Instructions" component={InstructionsSoundPlayer} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
