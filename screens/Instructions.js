import { useState, useEffect } from "react";
import { Audio } from "expo-av";
import { Text, View, TouchableOpacity } from "react-native";
import {
  NotificationFeedbackType,
  ImpactFeedbackStyle,
  notificationAsync,
  impactAsync,
} from "expo-haptics";
import { useNavigation } from "@react-navigation/native";

export default function InstructionsSoundPlayer({ finishCoordinate }) {
  const [sound, setSound] = useState();
  const navigation = useNavigation();

  async function playSound() {
    console.log("Loading Sound");
    const { sound } = await Audio.Sound.createAsync(
      require("../assets/sounds/music/instruction2.wav")
    );
    setSound(sound);

    console.log("Playing Sound");
    await sound.playAsync();
  }

  useEffect(() => {
    playSound();
    setTimeout(async function () {
      notificationAsync(NotificationFeedbackType.Warning);
    }, 17500);
  }, []);

  useEffect(() => {
    setTimeout(async function () {
      impactAsync(ImpactFeedbackStyle.Light);
    }, 23000);
  }, []);

  useEffect(() => {
    return sound
      ? () => {
          console.log("Unloading Sound");
          sound.unloadAsync();
        }
      : undefined;
  }, [sound]);

  return (
    <View style={{ flex: 1 }}>
      <TouchableOpacity
        style={{
          flex: 1,
          backgroundColor: "red",
          alignItems: "center",
          justifyContent: "center",
        }}
        onPress={() => {
          navigation.goBack();
        }}
      >
        <Text style={{ fontSize: 50 }}>Powrót</Text>
      </TouchableOpacity>
    </View>
  );
}
