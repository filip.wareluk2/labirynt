import React from "react";
import { View, TouchableOpacity } from "react-native";

export default function RestartLevel({ setMaze }) {
  return (
    <View
      style={{
        position: "absolute",
        justifyContent: "center",
        top: 70,
        backgroundColor: "#ffffff00",
      }}
    >
      <TouchableOpacity
        style={{ width: 500, height: 400 }}
        onPress={() => {
          setMaze();
        }}
      />
    </View>
  );
}
