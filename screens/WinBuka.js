import { StyleSheet, Text, View } from "react-native";

export default function WinBuka() {
  return (
    <View style={styles.container}>
      <Text style={styles.textStyle}>
        Przytuliłeś Bukę{"\n"}
        Wygrałeś :)</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "green"
  },
  textStyle: {
    fontSize: 50
  }
});
