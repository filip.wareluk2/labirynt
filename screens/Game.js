import React, { useState, useEffect } from "react";
import { StyleSheet, View, SafeAreaView, TouchableOpacity } from "react-native";
import BukaSoundPlayer from "../helper/bukaSoundPlayerHelper";
import MusichSoundPlayer from "../helper/musicSoundPlayerHelper";
import FinishSoundPlayer from "../helper/finishSoundPlayerHelper";
import calculateDistance from "../helper/positionHelper";
import readMaze from "../helper/mazeFilesHelper";
import movePlayer from "../helper/mazeMoveHelper";
import DisplayMaze from "../helper/mazeDisplayHelper";
import { config } from "../const/config";
import AccelerometerMoveHelper from "../helper/AccelerometerMoveHelper";
import { useNavigation } from "@react-navigation/native";
import RestartLevel from "./RestartLevel";
import {
  NotificationFeedbackType,
  ImpactFeedbackStyle,
  notificationAsync,
  impactAsync,
} from "expo-haptics";

import Icon from "react-native-vector-icons/MaterialIcons";

var mazeSize = config.mazeSize;

export default function Game({ route }) {
  const [playerPosition, setPlayerPosition] = useState({
    playerX: 0,
    playerY: 0,
  });
  const [level, setLevel] = useState(1);
  const [maze, setMaze] = useState(readMaze(level));
  const [movmentCallBack, setMovmentCallBack] = useState();
  const [restartReverse, setRestartReverse] = useState(false);
  const [bukaCoordinate, setBukaCoordinate] = useState([0, 0]);
  const [finishCoordinate, setFinishCoordinate] = useState([0, 0]);

  const navigation = useNavigation();

  var setLevelMain, setMazeMain, levelMain, mazeMain;

  useEffect(() => {
    setFinishCoordinate(GetCoordinatesFinish(maze));
    setBukaCoordinate(GetCoordinatesBuka(maze));
  }, [playerPosition]);

  if (route.params) {
    route.params.setMazeMain ? ({ setMazeMain } = route.params) : null;
    route.params.mazeMain ? ({ mazeMain } = route.params) : null;
    route.params.setLevelMain ? ({ setLevelMain } = route.params) : null;
    route.params.levelMain ? ({ levelMain } = route.params) : null;
  }

  useEffect(() => {
    if (restartReverse === true) {
      setMazeMain(readMaze(level));
      setTimeout(function () {
        impactAsync(ImpactFeedbackStyle.Medium);
        impactAsync(ImpactFeedbackStyle.Medium);
        navigation.goBack();
      }, 2000);
    }
  }, [restartReverse]);

  useEffect(() => {
    setMazeMain !== undefined ? setMazeMain(maze) : null;

    findPlayer({ maze, setPlayerPosition });
  }, [maze]);

  useEffect(() => {
    if (level != 1) {
      setLevelMain !== undefined ? setLevelMain(level) : null;
    }
  }, [level]);

  useEffect(() => {
    if (route.params) {
      if ((levelMain !== 1) & (levelMain !== undefined)) {
        setLevel(levelMain);
      }
      mazeMain ? setMaze(mazeMain) : null;
    }
  }, [route.params]);

  useEffect(() => {
    findPlayer({ maze, setPlayerPosition });
  }, []);

  useEffect(() => {
    setMovmentCallBack(undefined);
  }, [playerPosition]);

  useEffect(() => {
    let props = {
      playerPosition,
      setPlayerPosition,
      maze,
      setMaze,
      level,
      setLevel,
      navigation: (value) => {
        navigation.navigate(value);
      },
    };

    if (movmentCallBack !== undefined) {
      MotionControll({ direction: movmentCallBack, props });
    }
  }, [movmentCallBack]);

  return (
    <SafeAreaView style={styles.container}>
      <AccelerometerMoveHelper
        setMovmentCallBack={(value) => setMovmentCallBack(value)}
        setRestartReverse={(value) => setRestartReverse(value)}
      />
      <DisplayMaze arrey={maze} />
      <RestartLevel setMaze={() => setMaze(readMaze(level))} />
      <MotionControllContainer
        playerPosition={playerPosition}
        setPlayerPosition={(value) => setPlayerPosition(value)}
        maze={maze}
        setMaze={(value) => setMaze(value)}
        level={level}
        setLevel={(value) => setLevel(value)}
        navigation={(value) => {
          navigation.navigate(value);
        }}
      />
      <BukaSoundPlayer
        bukaCoordinate={bukaCoordinate}
        movmentCallBack={movmentCallBack}
      />
      <FinishSoundPlayer finishCoordinate={finishCoordinate} />
      <MusichSoundPlayer finishCoordinate={finishCoordinate} />
    </SafeAreaView>
  );
}

function MotionControllContainer(props) {
  return (
    <View style={styles.bottomContainer}>
      <View style={styles.buttonUpContainer}>
        <MotionControllButton direction={"u"} props={props} />
      </View>

      <View style={styles.buttonLeftRightContainer}>
        <MotionControllButton direction={"l"} props={props} />
        <MotionControllButton direction={"r"} props={props} />
      </View>
      <View style={styles.buttonDownContainer}>
        <MotionControllButton direction={"d"} props={props} />
      </View>
    </View>
  );
}

function MotionControllButton({ direction, props }) {
  return (
    <TouchableOpacity
      style={styles.commonButton}
      onPress={() => {
        MotionControll({ direction, props });
      }}
    >
      <Icon name={PickIconMotionButton(direction)} size={35} color={"white"} />
    </TouchableOpacity>
  );
}

function MotionControll({ direction, props }) {
  const {
    playerPosition,
    setPlayerPosition,
    maze,
    setMaze,
    level,
    setLevel,
    navigation,
  } = props;

  movePlayer(direction + "", props);
}

function PickIconMotionButton(direction) {
  let iconName;
  const icon = [
    { icon: "arrow-upward", direction: "u" },
    { icon: "arrow-back", direction: "l" },
    { icon: "arrow-forward", direction: "r" },
    { icon: "arrow-downward", direction: "d" },
  ];

  iconName = icon.filter((item) => {
    return item.direction.match(direction);
  });
  return iconName[0].icon;
}

function GetCoordinatesFinish(maze) {
  let coordinatesFinish = calculateDistance({ maze, objectType: "f" });
  return coordinatesFinish;
}

function GetCoordinatesBuka(maze) {
  let coordinatesBuka = calculateDistance({ maze, objectType: "b" });
  return coordinatesBuka;
}

function findPlayer({ maze, setPlayerPosition }) {
  for (let i = 0; i < mazeSize; i++) {
    for (let j = 0; j < mazeSize; j++) {
      if (maze[i][j] == "s") {
        setPlayerPosition({ playerX: j, playerY: i });
      }
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  bottomContainer: { flex: 2, width: "100%", alignItems: "center" },
  buttonUpContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  buttonLeftRightContainer: {
    flexDirection: "row",
    flex: 1,
    justifyContent: "space-evenly",
    width: "80%",
  },
  buttonDownContainer: { flex: 1, alignItems: "center" },
  commonButton: {
    width: 75,
    height: 75,
    backgroundColor: "black",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 15,
  },
});
