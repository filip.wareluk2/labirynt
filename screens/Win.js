import { StyleSheet, Text, View } from "react-native";

export default function Win() {
  return (
    <View style={styles.container}>
      <Text style={styles.textStyle}>Wygrałeś :)</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "yellow"
  },
  textStyle: {
    fontSize: 50
  }
});
