import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { useState } from "react";
import { useNavigation } from "@react-navigation/native";

export default function MainMenu() {
  const navigation = useNavigation();
  const [level, setLevel] = useState(1);
  const [maze, setMaze] = useState();


  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.gameButton}
        onPress={() => {
          navigation.navigate("Game", {
            setLevelMain: (value) => {
              setLevel(value);
            },
            setMazeMain: (value) => {
              setMaze(value);
            },
            levelMain: level,
            mazeMain: maze,
          });
        }}
      >
        <Text style={styles.textStyle}>Graj</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.instructionButton}
        onPress={() => {
          navigation.navigate("Instructions");
        }}
      >

        <Text style={styles.textStyle}>Instrukcja</Text>
      </TouchableOpacity>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  gameButton: {
    flex: 1,
    backgroundColor: "green",
    alignSelf: 'stretch',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 20
  },
  instructionButton: {
    flex: 1,
    backgroundColor: "red",
    alignSelf: 'stretch',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 20
  },
  textStyle: {
    fontSize: 50
  }
});
